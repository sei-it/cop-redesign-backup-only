﻿<%@ Page Title="" Language="C#" MasterPageFile="~/REMSCOPforum/AspNetForumMaster.Master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="remsUserinfo.aspx.cs" Inherits="REMSCOPforum_remsUserinfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHEAD" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AspNetForumContentPlaceHolder" Runat="Server">

<div style="text-align:right; padding-top:120px;">
    <asp:Button ID="btnReport" runat="server" onclick="ExportExcel" Text="Export report" />
</div>

    <asp:GridView ID="grdvwlist" runat="server" AutoGenerateColumns="False" 
        BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" 
        CellPadding="3" DataSourceID="SqlDataSource2" 
        AllowPaging="True" 
        onpageindexchanging="grdvwlist_PageIndexChanging" 
        onsorting="grdvwlist_Sorting">
        <Columns>
            <asp:BoundField DataField="aspnet_UserName" HeaderText="aspnet_UserName"
                SortExpression="aspnet_UserName" />
            <asp:BoundField DataField="institutionType" HeaderText="institutionType" 
                SortExpression="institutionType" />
            <asp:BoundField DataField="k12Role" HeaderText="k12Role" 
                SortExpression="k12Role" />
            <asp:BoundField DataField="higherEDRole" HeaderText="higherEDRole" 
                SortExpression="higherEDRole" />
            <asp:BoundField DataField="FirstName" HeaderText="FirstName" 
                SortExpression="FirstName" />
            <asp:BoundField DataField="LastName" HeaderText="LastName"
                SortExpression="LastName" />
            <asp:BoundField DataField="Email" HeaderText="Email" 
                SortExpression="Email" />
        </Columns>
        <FooterStyle BackColor="White" ForeColor="#000066" />
        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
        <RowStyle ForeColor="#000066" />
        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#007DBB" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#00547E" />
    </asp:GridView>

    <asp:GridView ID="gvReports" runat="server" AutoGenerateColumns="False" Visible="False"
        CellPadding="3" DataKeyNames="Userid" DataSourceID="SqlDataSource1" 
        BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
        <Columns>
            <asp:BoundField DataField="Userid" HeaderText="Userid" ReadOnly="True" 
                SortExpression="Userid" />
            <asp:BoundField DataField="aspnet_UserName" HeaderText="aspnet_UserName" 
                SortExpression="aspnet_UserName" />
            <asp:BoundField DataField="institutionType" HeaderText="institutionType" 
                SortExpression="institutionType" />
            <asp:BoundField DataField="k12Role" HeaderText="k12Role" 
                SortExpression="k12Role" />
            <asp:BoundField DataField="higherEDRole" HeaderText="higherEDRole" 
                SortExpression="higherEDRole" />
            <asp:BoundField DataField="otherRoleDes" HeaderText="otherRoleDes" 
                SortExpression="otherRoleDes" />
            <asp:BoundField DataField="FirstName" HeaderText="FirstName" 
                SortExpression="FirstName" />
            <asp:BoundField DataField="LastName" HeaderText="LastName" 
                SortExpression="LastName" />
            <asp:BoundField DataField="State" HeaderText="State" SortExpression="State" />
            <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
            <asp:BoundField DataField="SelectedType" HeaderText="SelectedType" 
                SortExpression="SelectedType" />
            <asp:BoundField DataField="yrExpEmergency" HeaderText="yrExpEmergency" 
                SortExpression="yrExpEmergency" />
            <asp:BoundField DataField="yrExpED" HeaderText="yrExpED" 
                SortExpression="yrExpED" />
            <asp:BoundField DataField="interests" HeaderText="interests" 
                SortExpression="interests" />
            <asp:CheckBoxField DataField="isAccept" HeaderText="isAccept" 
                SortExpression="isAccept" />
            <asp:BoundField DataField="CreatedOn" HeaderText="CreatedOn" 
                SortExpression="CreatedOn" />
        </Columns>
        <FooterStyle BackColor="White" ForeColor="#000066" />
        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
        <RowStyle ForeColor="#000066" />
        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#007DBB" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#00547E" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:REMSforumConnectionString %>" 
        SelectCommand="SELECT [Userid], [aspnet_UserName], [institutionType], [k12Role], [higherEDRole], [otherRoleDes], [FirstName], [LastName], [State], [City], [Email], [SelectedType], [yrExpEmergency], [yrExpED], [interests], [isAccept], [CreatedOn] FROM [remsUsers]">
    </asp:SqlDataSource>
     <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:REMSforumConnectionString %>" 
        SelectCommand="SELECT [aspnet_UserName], [institutionType], [k12Role], [higherEDRole], [FirstName], [LastName], [Email] FROM [remsUsers]">
    </asp:SqlDataSource>
</asp:Content>

