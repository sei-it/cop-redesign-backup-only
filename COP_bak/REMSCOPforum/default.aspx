<%@ Page language="c#" Codebehind="default.aspx.cs" AutoEventWireup="True" EnableViewState="true" Inherits="aspnetforum.forums" MasterPageFile="AspNetForumMaster.Master" %>
<%@ Register TagPrefix="cc" TagName="RecentPosts" Src="recentposts.ascx" %>


<asp:Content ContentPlaceHolderID="DefaultContentPlaceHolder" ID="AspNetForumContent" runat="server">
			<!--BEGIN MAIN CONTENT -->
			<div class="remsCOP-content" style="padding-top: 60px;">
				<section class="forumContent">
					<div class="container row clearfix">
						<div class="container ad">
                                <div class="grid_12 omega">
								    <div class="header-wrap">
									    <h2><%# Eval("GroupName") %></h2>
                                    </div>
                                    <div class="input-wrap">
                                    <asp:Button ID="btnAddnewForum" runat="server" Text="ADD A FORUM" Visible="false"
                                    onclick="btnAddnewForum_Click" CssClass="btn btn-info btn-lg" />

                                    <asp:DropDownList ID="ddlForumCat" runat="server" EnableViewState="true"
                                        AppendDataBoundItems="True" AutoPostBack="true"
                                        DataTextField="GroupName" DataValueField="GroupID" 
                                        onselectedindexchanged="ddlForumCat_SelectedIndexChanged">
                                        <asp:ListItem Value="0">SELECT A FORUM TO VIEW</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:AspNetForumConnectionString %>" 
                                        SelectCommand="SELECT [GroupID], [GroupName], [UserID] FROM [vwForumGroupbyUser] WHERE ([UserID] = @UserID)">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="hfUserID" Name="UserID" PropertyName="Value" 
                                                Type="Int32" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
								</div>
                                </div>
                        <asp:Repeater  ID="rptGroupsList" Runat="server" EnableViewState="False" OnItemDataBound="rptGroupsList_ItemDataBound">
		                    <ItemTemplate>
			                    <table style="width:100%;" class="roundedborder_disable biglist">
                                   <thead>
				                    <tr><th colspan="2" ><%= aspnetforum.Resources.various.ForumTitle %></th>
                                    <th><%= aspnetforum.Resources.various.Threads %></th>
                                    <th><%= aspnetforum.Resources.various.LatestPost %></th>
                                    </tr>
                                   </thead>
				                    <tbody>
			                    <asp:repeater id="rptForumsList" runat="server" EnableViewState="False">
				                    <ItemTemplate>
					                    <tr <%# Container.ItemType == ListItemType.AlternatingItem ? " class='altItem'" : "" %> >
						                    <td align="center" style="width:10%;border-right:none;"><img alt="" src="<%# GetForumIcon(Eval("IconFile")) %>" height="32" width="32" />
                                            </td>
						                    <td style="width:55%;border-left:none;padding-left: 0;"><h2>
							                    <a href='<%# aspnetforum.Utils.Various.GetForumURL(Eval("ForumID"), Eval("Title")) %>'><%# Eval("Title") %></a>
							                    </h2>
							                    <span class="gray2"><%# Eval("Description") %></span>
						                    </td>
						                    <td width="50" class="gray2" style="text-align: center">
							                    <%# Eval("TopicCount") %>
                                            </td>
						                    <td style="white-space:nowrap" class="gray2">
							                    <%# aspnetforum.Utils.Topic.GetTopicInfoBMessageyID(Eval("LatestMessageID"), Cmd)%>
                                            </td>
					                    </tr>
				                    </ItemTemplate>
			                    </asp:repeater>
			                    </table>
		                    </ItemTemplate>
		                    <FooterTemplate></tbody></FooterTemplate>
	                    </asp:Repeater>
	<div ID="lblNoForums" style="margin-top:20px;" runat="server" visible="false" enableviewstate="false"><%= aspnetforum.Resources.various.NoForums %></div>
	<div id="divNoForumsAdmin" style="margin-top:20px;" runat="server" visible="false">No forums have been created yet. Please go to the <a href="admin.aspx">administrator panel</a>.</div>


                        </div>
					</div>
				</section>
				<section class="community-huddles">
					<div class="container row clearfix">
						<div class="container ad">
							<div class="grid_12 omega">
								<div class="grid_1 omega"> <i class="fa fa-map-marker"></i> </div>
								<div class="grid_11 omega">
									<h2>Our Private Groups are now called Community Huddles!</h2> </div>
								<div class="grid_12 omega">
									<div class="community-btn-wrap">
										<p class="pad-top">Community Huddles allow you to create or join conversations and topics specific to smaller, more focused groups within the nationwide Community of Practice.</p>
										<ul>
											<%--<li><a class="btn btn-info btn-lg col-xs-12" href="default.aspx?gid=<%# getHuddleID() %>">VIEW EXISTING COMMUNITY HUDDLES</a></li>--%>
											<li><a class="btn btn-info btn-lg col-xs-12" href="#" onclick="ShowMessage();">START MY OWN COMMUNITY HUDDLE</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
                <table style="width:100%; text-align:center;" cellpadding="11" cellspacing="0" class="roundedborder">
		<tr>
        <th style="text-align:center;"><h2><%= aspnetforum.Resources.various.WhatsGoingOn %></h2></th>
        </tr>
		<tbody>
	<tr>
		<td>
			<span class="gray"><%= aspnetforum.Resources.various.UsersOnline %></span>
			<%= aspnetforum.Utils.User.OnlineUsersCount %>&nbsp;&nbsp;
			<span class="gray"><%= aspnetforum.Resources.various.Members %></span>
			<%= aspnetforum.Utils.User.OnlineRegisteredUsersCount %>&nbsp;&nbsp;
			<span class="gray"><%= aspnetforum.Resources.various.Guests %></span>
			<%= aspnetforum.Utils.User.OnlineUsersCount-aspnetforum.Utils.User.OnlineRegisteredUsersCount%>
			<br /><br />
			<span class="gray"><%= aspnetforum.Resources.various.Threads %></span>
			<%= aspnetforum.Utils.Various.GetStats().ThreadCount %>&nbsp;&nbsp;
			<span class="gray"><%= aspnetforum.Resources.various.Posts %></span>
			<%= aspnetforum.Utils.Various.GetStats().PostCount %>&nbsp;&nbsp;
			<span class="gray"><%= aspnetforum.Resources.various.Members %></span>
			<%= aspnetforum.Utils.Various.GetStats().MemberCount %>
		</td>
	</tr>
			</tbody>
	</table>

	<div id="divRecent" runat="server" enableviewstate="false" visible="false">
	<br />
	<cc:RecentPosts id="recentPosts" runat="server"></cc:RecentPosts>
	</div>
			</div>
			<!--END MAIN CONTENT-->


    <asp:HiddenField ID="hfUserID" runat="server" />
	
	
    <script type="text/javascript">
        function ShowMessage() { alert("To start your own Community Huddle, email us at info@remstacenter.org with a suggested title, description and list of invited participants."); }
    
    </script>

</asp:Content>