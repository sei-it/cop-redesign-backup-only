﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsForumMasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
    <!--BEGIN MAIN CONTENT -->
			<div class="remsCOP-content">
				<!--BEGIN FIRST SECTION-->
				<section>
					<div class="container row clearfix">
						<div class="container ad">
							<div class="grid_12 omega">
								<h2>Where Partners in School and Higher Ed Safety Come Together</h2> </div>
							<!-- /grid_12 omega -->
							<div class="grid_12 omega">
								<p>The REMS TA Center CoP is a collaborative of practitioners with the collective aim to enhance the ability of schools, school districts, IHEs, state education agencies (SEAs), and their community partners to develop high-quality emergency operations plans (EOPs) and implement comprehensive emergency management planning efforts through the sharing of ideas, experiences, lessons learned, and by engaging with one another on these important topics.</p>
							</div>
							<!-- /grid_12 omega -->
							<div class="grid_12 omega">
								<div class="tabs-container">
									<!-- Nav tabs -->
									<ul class="nav nav-tabs" role="tablist">
										<li role="presentation" class="active"><a href="#collaborate" aria-controls="collaborate" role="tab" data-toggle="tab"><span class="fa fa-cogs"></span> Collaborate</a></li>
										<li role="presentation"><a href="#share" aria-controls="share" role="tab" data-toggle="tab"><span class="fa fa-comments-o"></span> Share</a></li>
										<li role="presentation"><a href="#getinspired" aria-controls="getinspired" role="tab" data-toggle="tab"><span class="fa fa-lightbulb-o"></span> Get Inspired</a></li>
										<li role="presentation"><a href="#support" aria-controls="support" role="tab" data-toggle="tab"><span class="fa fa-life-bouy"></span> Get Support</a></li>
									</ul>
									<!-- Tab panes -->
									<div class="tab-content" id="copTabs">
										<div role="tabpanel" class="tab-pane active" id="collaborate">
											<p>Create or join a private group to initiate conversations and topics specific to smaller, more focused groups within the nationwide CoP.</p>
										</div>
										<div role="tabpanel" class="tab-pane" id="share">
											<p> Share success stories and safety tips based on real-world experiences with school and IHE emergencies, and offer recommendations for recovery efforts. </p>
										</div>
										<div role="tabpanel" class="tab-pane" id="getinspired">
											<p><a href="#">View lessons learned and see what other schools and IHEs are doing.</a></p>
										</div>
										<div role="tabpanel" class="tab-pane" id="support">
											<p>Chat with <a href="#">REMS TA Center staff</a> or request FREE technical assistance in a variety of emergency management areas.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="grid_12 omega">
								<p class="pad-top">Anyone involved in the field of school and/or IHE emergency management is welcome to join, from members of emergency operations planning teams and law enforcement officers to community members and parents.</p>
							</div>
							<div class="grid_12 omega"> <a class="btn btn-info btn-lg" href="#"><i class="fa fa-user-plus"></i> Join</a> <a class="btn btn-info btn-lg" href="#"><i class="fa fa-info-circle"></i> Learn More</a> <a class="btn btn-info btn-lg" href="#"><i class="fa fa-sign-in"></i> Log In</a> </div>
						</div>
						<!-- /container -->
					</div>
					<!-- /container row CoP clearfix -->
				</section>
				<!-- END FIRST SECTION -->
				<!-- BEGIN TRENDING TOPICS-->
				<section class="trending-topics">
					<div class="container row clearfix">
						<div class="container ad">
							<div class="grid_12 omega">
								<h2>Trending Topics</h2> </div>
							<div class="grid_12 omega">
								<p>A sneak peek of some of the topics currently being discussed in our forums. There are many categories and topics. <a href="#">Sign up</a> to see more!</p>
							</div>
							<div class="grid_12 omega">
								<div class="carousel-wrapper">
									<div id="trendingTopics" class="carousel slide" data-ride="carousel">
										<!-- Indicators -->
										<ol class="carousel-indicators">
											<li data-target="#trendingTopics" data-slide-to="0" class="active"></li>
											<li data-target="#trendingTopics" data-slide-to="1"></li>
											<li data-target="#trendingTopics" data-slide-to="2"></li>
											<li data-target="#trendingTopics" data-slide-to="3"></li>
											<li data-target="#trendingTopics" data-slide-to="4"></li>
										</ol>
										<!-- Wrapper for slides -->
										<div class="carousel-inner" role="listbox">
											<div class="item active">
												<div class="topics-box">
													<h3>FY2015 Comprehensive School Safety Initiative</h3>
													<div class="grid_2"> <img src="images/new/profile-placeholder.gif" class="img-responsive" alt="" /> </div>
													<div class="grid_9">
														<p>The U.S. Department of Justice, Office of Justice Programs, National Institute of Justice (NIJ), is accepting applications for the FY2015 Comprehensive School Safety Initiative (CSSI). The goal of this initiative is to improve school safety by providing communities with best practices for programs and policies based on rigorous scientific research, testing and evaluation.</p>
														<div class="grid_12 omega"> <a class="btn btn-info btn-lg" href="#">Read More <i class="fa fa-angle-double-right"></i></a> </div>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="topics-box">
													<h3>Topic Two </h3>
													<div class="grid_2"> <img src="images/new/profile-placeholder.gif" class="img-responsive" alt="" /> </div>
													<div class="grid_9">
														<p>Lorem ipsum dolor sit amet, ante pretium egestas ac nunc, a tempor interdum orci amet ut nullam, cras iaculis, urna lacus litora metus. Sed eros in. Ligula consequat nec lobortis ac sed lacus, iure dictumst purus mauris, nec curabitur placerat sem risus felis praesent. Lobortis orci.</p>
													</div>
													<div class="grid_12 omega"> <a class="btn btn-info btn-lg" href="#">Read More <i class="fa fa-angle-double-right"></i></a> </div>
												</div>
											</div>
											<div class="item">
												<div class="topics-box">
													<h3>Topic Three</h3>
													<div class="grid_2"> <img src="images/new/profile-placeholder.gif" class="img-responsive" alt="" /> </div>
													<div class="grid_9">
														<p>Lorem ipsum dolor sit amet, ante pretium egestas ac nunc, a tempor interdum orci amet ut nullam, cras iaculis, urna lacus litora metus. Sed eros in. Ligula consequat nec lobortis ac sed lacus, iure dictumst purus mauris, nec curabitur placerat sem risus felis praesent. Lobortis orci.</p>
													</div>
													<div class="grid_12 omega"> <a class="btn btn-info btn-lg" href="#">Read More <i class="fa fa-angle-double-right"></i></a> </div>
												</div>
											</div>
											<div class="item">
												<div class="topics-box">
													<h3>Topic Four</h3>
													<div class="grid_2"> <img src="images/new/profile-placeholder.gif" class="img-responsive" alt="" /> </div>
													<div class="grid_9">
														<p>Lorem ipsum dolor sit amet, ante pretium egestas ac nunc, a tempor interdum orci amet ut nullam, cras iaculis, urna lacus litora metus. Sed eros in. Ligula consequat nec lobortis ac sed lacus, iure dictumst purus mauris, nec curabitur placerat sem risus felis praesent. Lobortis orci.</p>
													</div>
													<div class="grid_12 omega"> <a class="btn btn-info btn-lg" href="#">Read More <i class="fa fa-angle-double-right"></i></a> </div>
												</div>
											</div>
											<div class="item">
												<div class="topics-box">
													<h3>Topic Five</h3>
													<div class="grid_2"> <img src="images/new/profile-placeholder.gif" class="img-responsive" alt="" /> </div>
													<div class="grid_9">
														<p>Lorem ipsum dolor sit amet, ante pretium egestas ac nunc, a tempor interdum orci amet ut nullam, cras iaculis, urna lacus litora metus. Sed eros in. Ligula consequat nec lobortis ac sed lacus, iure dictumst purus mauris, nec curabitur placerat sem risus felis praesent. Lobortis orci.</p>
													</div>
													<div class="grid_12 omega"> <a class="btn btn-info btn-lg" href="#">Read More <i class="fa fa-angle-double-right"></i></a> </div>
												</div>
											</div>
										</div>
										<!-- Left and right controls -->
										<a class="left carousel-control" href="#trendingTopics" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a>
										<a class="right carousel-control" href="#trendingTopics" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- END TRENDING TOPICS -->
				<!-- BEGIN COMMUNITY SPOTLIGHT -->
				<section class="community-spotlight">
					<div class="container row clearfix">
						<div class="container ad">
							<div class="grid_12 omega">
								<h2>Community Spotlight</h2> </div>
							<div class="grid_12 omega">
								<div class="carousel-wrapper">
									<div id="communitySpotlight" class="carousel slide" data-ride="carousel">
										<!-- Indicators -->
										<ol class="carousel-indicators">
											<li data-target="#communitySpotlight" data-slide-to="0" class="active"></li>
											<li data-target="#communitySpotlight" data-slide-to="1"></li>
											<li data-target="#tcommunitySpotlight" data-slide-to="2"></li>
											<li data-target="#communitySpotlight" data-slide-to="3"></li>
											<li data-target="#communitySpotlight" data-slide-to="4"></li>
										</ol>
										<!-- Wrapper for slides -->
										<div class="carousel-inner" role="listbox">
											<div class="item active">
												<div class="spotlight-box">
													<div class="grid_12"> <img src="images/new/profile-placeholder.gif" class="img-responsive" alt="" /> </div>
													<div class="grid_12">
														<div class="spotlight-text">
															<h3>FY2015 Comprehensive School Safety Initiative</h3>
															<p>The U.S. Department of Justice, Office of Justice Programs, National Institute of Justice (NIJ), is accepting applications for the FY2015 Comprehensive School Safety Initiative (CSSI). The goal of this initiative is to improve school safety by providing communities with best practices for programs and policies based on rigorous scientific research, testing and evaluation.</p> <a class="btn btn-info btn-lg" href="#">Read More <i class="fa fa-angle-double-right"></i></a> </div>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="spotlight-box">
													<div class="grid_12"> <img src="images/new/profile-placeholder.gif" class="img-responsive" alt="" /> </div>
													<div class="grid_12">
														<div class="spotlight-text">
															<h3>Topic Two</h3>
															<p>Lorem ipsum dolor sit amet, ante pretium egestas ac nunc, a tempor interdum orci amet ut nullam, cras iaculis, urna lacus litora metus. Sed eros in. Ligula consequat nec lobortis ac sed lacus, iure dictumst purus mauris, nec curabitur placerat sem risus felis praesent. Lobortis orci.</p> <a class="btn btn-info btn-lg" href="#">Read More <i class="fa fa-angle-double-right"></i></a> </div>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="spotlight-box">
													<div class="grid_12"> <img src="images/new/profile-placeholder.gif" class="img-responsive" alt="" /> </div>
													<div class="grid_12">
														<div class="spotlight-text">
															<h3>Topic Three</h3>
															<p>Lorem ipsum dolor sit amet, ante pretium egestas ac nunc, a tempor interdum orci amet ut nullam, cras iaculis, urna lacus litora metus. Sed eros in. Ligula consequat nec lobortis ac sed lacus, iure dictumst purus mauris, nec curabitur placerat sem risus felis praesent. Lobortis orci.</p> <a class="btn btn-info btn-lg" href="#">Read More <i class="fa fa-angle-double-right"></i></a> </div>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="spotlight-box">
													<div class="grid_12"> <img src="images/new/profile-placeholder.gif" class="img-responsive" alt="" /> </div>
													<div class="grid_12">
														<div class="spotlight-text">
															<h3>Topic Four</h3>
															<p>Lorem ipsum dolor sit amet, ante pretium egestas ac nunc, a tempor interdum orci amet ut nullam, cras iaculis, urna lacus litora metus. Sed eros in. Ligula consequat nec lobortis ac sed lacus, iure dictumst purus mauris, nec curabitur placerat sem risus felis praesent. Lobortis orci.</p> <a class="btn btn-info btn-lg" href="#">Read More <i class="fa fa-angle-double-right"></i></a> </div>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="spotlight-box">
													<div class="grid_12"> <img src="images/new/profile-placeholder.gif" class="img-responsive" alt="" /> </div>
													<div class="grid_12">
														<div class="spotlight-text">
															<h3>Topic Five</h3>
															<p>Lorem ipsum dolor sit amet, ante pretium egestas ac nunc, a tempor interdum orci amet ut nullam, cras iaculis, urna lacus litora metus. Sed eros in. Ligula consequat nec lobortis ac sed lacus, iure dictumst purus mauris, nec curabitur placerat sem risus felis praesent. Lobortis orci.</p> <a class="btn btn-info btn-lg" href="#">Read More <i class="fa fa-angle-double-right"></i></a> </div>
													</div>
												</div>
											</div>
											<div class="btn-wrapper"> <span aria-hidden="true"><a class="fa fa-arrow-circle-left" id="backBtn"></a></span> <span aria-hidden="true"><a class="fa fa-arrow-circle-right" id="fwdBtn"></a></span> </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- END COMMUNITY SPOTLIGHT -->
			</div>
	<!--END MAIN CONTENT-->
</asp:Content>

