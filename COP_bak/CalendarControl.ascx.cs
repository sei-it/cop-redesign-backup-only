﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Text;
using System.ComponentModel;
using System.Security.Permissions;

namespace EVEA.project
{
    [
        AspNetHostingPermission(SecurityAction.Demand,
            Level = AspNetHostingPermissionLevel.Minimal),
        AspNetHostingPermission(SecurityAction.InheritanceDemand,
            Level = AspNetHostingPermissionLevel.Minimal),
        ToolboxData("<{0}:IndexButton runat=\"server\"> </{0}:IndexButton>")
    ]
    public partial class CalendarControl : System.Web.UI.UserControl
    {
        remsUsersDataClassesDataContext db = new remsUsersDataClassesDataContext();
        private int m_Month;
        [
                Bindable(true),
                Category("Behavior"),
                DefaultValue(1),
                Description("Calendar month.")
        ]
        public int Month
        {
            get
            {
                return m_Month;
            }
            set
            {
                m_Month = value;
            }
        }
        private int m_Year;
        [
                Bindable(true),
                Category("Behavior"),
                DefaultValue(1990),
                Description("Calendar year.")
        ]
        public int Year
        {
            get
            {
                return m_Year;
            }
            set
            {
                m_Year = value;
            }
        }
        private int m_GroupID = 0;
        [
                Bindable(true),
                Category("Behavior"),
                DefaultValue(0),
                Description("Content group.")
        ]
        public int GroupID
        {
            get
            {
                return m_GroupID;
            }
            set
            {
                m_GroupID = value;
            }
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            Page.RegisterRequiresControlState(this);
        }

        protected override object SaveControlState()
        {
            object obj = base.SaveControlState();

            string str = m_Month.ToString() + ";" + m_Year.ToString() + ";" + m_GroupID.ToString();
            if (obj != null)
            {
                return new Pair(obj, str);
            }
            else
            {
                return (str);
            }
        }

        protected override void LoadControlState(object state)
        {
            if (state != null)
            {
                Pair p = state as Pair;
                if (p != null)
                {
                    base.LoadControlState(p.First);
                    string str = (string)p.Second;
                    string[] strs = str.Split(';');
                    m_Month = Convert.ToInt32(strs[0]);
                    m_Year = Convert.ToInt32(strs[1]);
                    m_GroupID = Convert.ToInt32(strs[2]);
                }
                else
                {
                    if (state is string)
                    {
                        string str = (string)state;
                        string[] strs = str.Split(';');
                        m_Month = Convert.ToInt32(strs[0]);
                        m_Year = Convert.ToInt32(strs[1]);
                        m_GroupID = Convert.ToInt32(strs[2]);
                    }
                    else
                    {
                        base.LoadControlState(state);
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                LoadEvents(m_Month, m_Year, m_GroupID);
        }
        protected void OnPreviousMonth(object sender, EventArgs e)
        {
            DateTime dt = new DateTime(m_Year, m_Month, 1);
            dt = dt.AddMonths(-1);
            m_Month = dt.Month;
            m_Year = dt.Year;
            LoadEvents(m_Month, m_Year, m_GroupID);
        }

        protected void OnNextMonth(object sender, EventArgs e)
        {
            DateTime dt = new DateTime(m_Year, m_Month, 1);
            dt = dt.AddMonths(1);
            m_Month = dt.Month;
            m_Year = dt.Year;
            LoadEvents(m_Month, m_Year, m_GroupID);
        }
        public void LoadEvents(int month, int year, int groupID)
        {
            string[] monthes = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            DateTime dt = new DateTime(year, month, 1); ;
            DateTime nextMonth = dt.AddMonths(1);

            int idx = 0;

            StringBuilder sb = new StringBuilder();
            //sb.AppendFormat("<table class='Calendar' cellpadding='0' cellspacing='0' ><tr><td colspan='7' align='center'>{0} {1}</td></tr>", monthes[month - 1], year.ToString());
            lblMonth.Text = monthes[month - 1] + " " + year.ToString();
            sb.AppendFormat("<table class='Calendar' cellpadding='0' cellspacing='0' >");
            sb.Append("<tr class='CalendarRow'><th class='CalendarCell'>Sun</th><th class='CalendarCell'>Mon</th><th class='CalendarCell'>Tue</th><th class='CalendarCell'>Wed</th>");
            sb.Append("<th class='CalendarCell'>Thur</th><th class='CalendarCell'>Fri</th><th class='CalendarCell'>Sat</th></tr>");
            sb.Append("<tr class='CalendarRow'>");
            for (int i = 0; i < (int)dt.DayOfWeek; i++)
            {
                sb.Append("<td>&nbsp;</td>");
                idx++;
            }
            while (dt.CompareTo(nextMonth) < 0)
            {
                //var events = db.Calendars.All(x => (((DateTime)x.CalendarStart).CompareTo(dt) >= 0 && ((DateTime)x.CalendarStart).CompareTo(dt.AddDays(1)) < 0)
                //    || (((DateTime)x.CalendarEnd).CompareTo(dt) >= 0 && ((DateTime)x.CalendarStart).CompareTo(dt.AddDays(1)) < 0));
                var events = from evt in db.Calendars
                             where (((DateTime)evt.CalendarStart).CompareTo(dt) >= 0 && ((DateTime)evt.CalendarStart).CompareTo(dt.AddDays(1)) < 0)
                             || (((DateTime)evt.CalendarEnd).CompareTo(dt) >= 0 && ((DateTime)evt.CalendarStart).CompareTo(dt.AddDays(1)) < 0)
                             select evt;
                
                //if (events.Count() > 0)
                //{

                if (events.Count() > 0)
                {
                    string eventIDs = "", evtTitle="";
                    foreach (Calendar calendar in events)
                    {
                        eventIDs += calendar.ID.ToString() + ";";
                        evtTitle += calendar.CalendarTitle.ToString().Split(':')[1] + "<br/><hr/>";
                    }
                    evtTitle = evtTitle.Substring(0, evtTitle.Length -10);
                    eventIDs = eventIDs.Substring(0, eventIDs.Length - 1);
                    sb.AppendFormat("<td class='CalendarEventCell' onMouseover=\"ddrivetip('" + evtTitle + "', 'yellow', 250)\" onMouseout=\"hideddrivetip()\" onClick=\"tb_show('Event Details','eventdetail.aspx?id={1}&height=250&width=250','');\" Style='cursor:pointer'>{0}</td>", dt.Day, eventIDs);
                    //sb.AppendFormat("<a href='#' onclick=\"tb_show('Event Details','eventdetail.aspx?id={1}&height=250&width=250','');\" >{0}</a></td>", dt.Day, eventIDs);
                }
                else
                {
                    sb.AppendFormat("<td class='CalendarCell'></div>{0}</td>", dt.Day);
                }
                //}
                //else
                //{
                //    sb.AppendFormat("<td class='CalendarCell'>{0}</td>", dt.Day);
                //}
                idx++;
                if (idx > 6)
                {
                    sb.Append("</tr><tr class='CalendarRow'>");
                    idx -= 7;
                }
                dt = dt.AddDays(1);
            }

            for (int i = idx; i <= 6; i++)
            {
                sb.Append("<td>&nbsp;</td>");
            }
            sb.Append("</tr>");

            sb.Append("</table>");

            CalendarDiv.InnerHtml = sb.ToString();
        }
        private int CompareDate(DateTime dt, DateTime cdt)
        {
            DateTime dt1 = new DateTime(dt.Year, dt.Month, dt.Day);
            DateTime dt2 = new DateTime(cdt.Year, cdt.Month, cdt.Day);

            return dt1.CompareTo(dt2);
        }
    }
}