﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsForumMasterPage.master" AutoEventWireup="true" CodeFile="3_PortalRules.aspx.cs" Inherits="_3_PortalRules" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentLeftMenu" Runat="Server">
    <div class="column left forumleft">
<h4 class="sideMenu">Community of Practice</h4>
<ul>
<li><asp:LinkButton ID="LinkButton5" PostBackUrl="~/Default.aspx" runat="server">Home</asp:LinkButton></li>
<li><asp:LinkButton ID="LinkButton2" PostBackUrl="~/2_About.aspx" runat="server">About the REMS TA Center CoP</asp:LinkButton></li>
<%--<li><asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/AboutTeam.aspx">Meet the REMS TA Center Team</asp:LinkButton></li>--%>
<li><asp:Label ID="lblid3" ForeColor="#85952A" runat="server"><strong>Community Rules</strong></asp:Label></li>
<li><asp:LinkButton ID="LinkButton7" runat="server" PostBackUrl="~/Account/Register.aspx">join the community</asp:LinkButton></li>
<li><asp:LinkButton ID="lbtnCOPindex" runat="server" Visible="false" PostBackUrl="~/REMSCOPforum/COPindex.aspx">Community Forums</asp:LinkButton></li>
<li><asp:LinkButton ID="lbtnloginout" PostBackUrl="~/Account/Login.aspx?ReturnUrl=../REMSCOPforum/COPindex.aspx" runat="server" Text="Log in"></asp:LinkButton></li>
<%--<li><asp:LinkButton ID="LinkButton1" onClientClick="window.open('events.aspx');" runat="server">Events</asp:LinkButton></li>--%>
</ul>
</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
    <%--<div class="main3x">--%>
    <div class="main2x">
    <h1>
REMS TA Center Community Rules
    </h1>
    <p>
        The REMS TA Center Community of Practice (CoP) is a virtual space for schools, school districts, institutions of higher education (IHEs), and their community partners to collaborate on special projects, share news and resources, discuss trends and ideas, and learn from the experiences of others in the field. As a forum for practitioners in the field, content on the REMS TA Center CoP might come from various sources and might be based on the experiences of CoP members and nonmembers. Statements, opinions, resources, or any other submissions posted to the CoP by a member are not necessarily endorsed by the U.S. Department of Education (ED) or the REMS TA Center. ED and the REMS TA Center do not support the selling of products, services, or goods by vendors in the CoP. 
    </p>
    <p>
       Please remember that the community will reflect and be enhanced by the diversity of those who support our nation’s schools and IHEs. At times, community members might not share the same opinion on a topic. Our Codes of Conduct and Terms of Use are designed to ensure that all community members receive respect from their fellow members while participating. Please read and accept our Codes of Conduct and Terms of Use to ensure a productive and healthy experience for all community members. 
    </p>
    <h2>CODES OF CONDUCT:</h2>
    <ul>
        <li><strong>Think twice before posting confidential or sensitive information</strong>. Our goal is to ensure the privacy and security of all CoP members. However, the REMS TA Center cannot guarantee that knowledge and information exchanged will not be shared outside the CoP. Protect yourself and others, and refrain from posting sensitive information that could cause harm if viewed outside of the CoP.</li>
        <li><strong>You are accountable for messages posted to the CoP</strong>. To ensure that you (and not your school, district, IHE, or agency) are responsible for content posted, we require that all members use their real name and official affiliation.</li>
        <li><strong>A strong and diverse community requires mutual respect</strong>. The REMS TA Center encourages CoP members to participate in forum discussions and debates. We ask that the opinions and thoughts of all members be respected. Please also consider that the CoP is open to a diverse audience, and some members might not be communicating in their primary language.</li>
    </ul>
    <h2>TERMS OF USE:</h2>
    <ol>
        <li>ED and the REMS TA Center do not guarantee or warrant that any information posted by individuals on these pages is correct and disclaim any liability for any loss or damage resulting from reliance on any such information. ED and the REMS TA Center may not be able to verify, do not warrant or guarantee, and assume no liability for anything posted on this website by any other person. ED and the REMS TA Center do not endorse, support, or otherwise promote any private or commercial entity or the information, products, or services contained on those Web sites that may be reached through links on our Web site. Opinions and comments that appear in the CoP belong to the individuals who expressed them. They do not belong to or represent views of ED or the REMS TA Center.</li>    
        <li>These pages may not be used for the submission of any claim, demand, informal or formal complaint, or any other form of legal and/or administrative notice or process, or for the exhaustion of any legal and/or administrative remedy.</li>  
        <li>REMS TA Center CoP members are solely responsible for ensuring that they do not act in any manner that constitutes or forms a part of a course of conduct amounting to a violation of any state, federal, international, or other applicable law. This includes, but is not limited to, posting of content in violation of the copyright on that content.</li>  
        <li>The REMS TA Center does not allow content that is abusive, vulgar, racist, sexist, slanderous, harassing, misleading, or otherwise objectionable. We also do not allow content that promotes the selling of products, goods, and services. Content of this nature will be screened and removed. </li>  
    </ol>
    <h2>
        TERMINATION:
    </h2>
    <p>
        The U.S. Department of Education and REMS TA Center may terminate or suspend your access to all or part of the REMS TA Center CoP, including but not limited to any discussion forums on its site, for any reason, including breach of the Terms of Use or Codes of Conduct. If you are unsatisfied with the services provided by the REMS TA Center, please email <a href="mailto:info@remstacenter.org">info@remstacenter.org</a> to terminate your membership.
    </p>
    <h2>PRIVACY POLICY:</h2>
    <p>
The U.S. Department of Education and REMS TA Center will not sell any personal information about REMS TA Center CoP members to any third parties. The U.S. Department of Education and REMS TA Center may use account information generically in order to revise its services or present findings from the use of its services. 
</p>
   <%-- <asp:RadioButtonList ID="rblAccept" runat="server">
        <asp:ListItem Value="1">I accept the Terms of Use and Codes of Conduct.</asp:ListItem>
        <asp:ListItem Value="0">I do not accept the Terms of Use and Codes of Conduct.</asp:ListItem>
    </asp:RadioButtonList>--%>
    </div>
    
   <div runat="server" visible="false" class="columnSmall right"> 
				<div id="newsRotator" class="featureBox">
                    <a href="#" onclick="return false;" class="control_next">&nbsp;</a>
                    <a href="#" onclick="return false;" class="control_prev">&nbsp;</a>
						<h3 class="featureHead icon_cal">
							<%--News &amp; Highlights</h3>--%>
                          <a href="NewsandHighlights.aspx">News &amp; Highlights</a></h3>
					  <ul>
						
                         <li>
                        <img src="../../images/humanTrafficking.jpg" alt="Human Trafficking 101 for School Administrators and Staff" />
                        <h3>Human Trafficking 101 for School Administrators and Staff</h3>
                         <p><a href="docs/Human Trafficking 101 for School Administrators and Staff.pdf" target="_blank">Download the Report</a></p>
                      </li>

                        <li>
                        <img src="../../images/colleagueLetter.jpg" alt="Man reading letter on tablet device" />
                        <h3><a href="http://www.ifap.ed.gov/dpcletters/GEN1413.html" target="_blank">Guidance to Institutions on Compliance with Amended Clery Act</a></h3>
                        <p><a href="http://www.ifap.ed.gov/dpcletters/GEN1413.html" target="_blank">Download the Dear Colleague Letter</a></p>
                      </li>
                      
                      <li>
                        <img src="../../images/confrontingExploitation.jpg" alt="Screen capture from promotional video" />
                        <h3><a href="http://www.iom.edu/Reports/2013/Confronting-Commercial-Sexual-Exploitation-and-Sex-Trafficking-of-Minors-in-the-United-States.aspx" target="_blank">Confronting Commercial Sexual Exploitation and Sex Trafficking of Minors in the U.S.</a></h3>
                        <p><a href="http://www.iom.edu/Reports/2013/Confronting-Commercial-Sexual-Exploitation-and-Sex-Trafficking-of-Minors-in-the-United-States.aspx" target="_blank">Download Report Here</a>​</p>
                      </li>
                      
						<li>
                        <img src="../../images/collegeSafety-1.jpg" alt="College students in common area of campus" />
                        <h3>Keeping Campuses Safe</h3>
                        <p>U.S. Department of Education Announces Action to <a href="http://www.ed.gov/news/press-releases/us-department-education-announces-action-help-colleges-keep-campuses-safe" target="_blank">Help Colleges Keep Campuses Safe</a></p>
                      </li>
                        
						<li>
                        	<img src="../../images/schoolCrime2013.gif" alt="2013 School Crime and Saftey Report" >
                            <h3><a href="http://www.bjs.gov/content/pub/pdf/iscs13.pdf" target="_blank">New 2013 School Crime and Safety Report Released</a></h3>
                            <p><a href="http://www.bjs.gov/content/pub/pdf/iscs13.pdf" target="_blank">Download the Full Report</a></p>
                      </li>
                        
						<li>
                        <img src="../../images/vftf-banner.gif" alt="Voices from the Field" >
                        <h3><a href="http://safesupportivelearning.ed.gov/voices-field/which-activity-most-helpful-gauging-student-perception-your-schooldistrictcommunity" target="_blank">Share Your Voice from the Field on Student Perception</a></h3>
                        <p><a href="http://safesupportivelearning.ed.gov/voices-field/which-activity-most-helpful-gauging-student-perception-your-schooldistrictcommunity" target="_blank">Respond to a Survey and Share Experiences</a></p>
                      </li>

                      <li>
                      <img src="../../images/PreventingYouthViolence.jpg" alt="Preventing Youth Violence banner" >
                      <h3><a href="http://www.cdc.gov/violenceprevention/youthviolence/Opportunities-for-Action.html" target="_blank">HHS/CDC Releases Preventing Youth Violence: Opportunities for Action </a></h3>
                      <p><a href="http://www.cdc.gov/violenceprevention/youthviolence/Opportunities-for-Action.html" target="_blank">Download the Full Report</a></p>
                      </li>
        

					</ul>

                    <div class="slider_option">
                      <input type="checkbox" id="checkbox" checked="checked" />
                      <label for="checkbox">Autoplay Slider</label>
                    </div> 
                    
                    
				</div>
  

                   <%--  <h3>Leadership Meeting on School Safety, Security, and Emergency Management</h3>
                     <p><a href="leadershipmtg.aspx">Materials Available Here</a></p>--%>


        
              
                 
                 
             
                 
                   
<div class="featureBox">
						<h3 class="featureHead icon_page">
							Quick Links</h3>

                             <%--<asp:Literal ID="LiteralQuickLinks" runat="server"></asp:Literal>		--%>	           
                    <ul>
                        <li><a href="TA_TrainingsByRequest.aspx">Request a Training</a></li> 
                        <li><a href="RequestTA.aspx">Request TA</a></li> 
                        <li><a href="TA_VirtualTrainings.aspx">Take a Webinar</a></li>
                        <li><a href="Resources.aspx">Find a Resource</a></li>
                        <li><a href="stateresources.aspx">Access State Emergency Management Resources</a></li>
                        <li><a href="granteecorner.aspx">Grantee Corner</a></li>
                        <li><a href="banners.aspx">Download Creative Materials</a></li>
					</ul>
                             		
				</div>
    </div> <!-- / column right --> 
    
</asp:Content>

