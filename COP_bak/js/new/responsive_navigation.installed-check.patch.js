diff --git a/responsive_navigation.install b/responsive_navigation.install
index d3f67f4..2426ad3 100644
--- a/responsive_navigation.install
+++ b/responsive_navigation.install
@@ -44,9 +44,11 @@ function responsive_navigation_requirements($phase) {
   if ($phase == 'runtime') {
     $t = get_t();
     $requirements['responsive_navigation']['title'] = t('responsive-nav.js library');
+    $version = $t('Installed');
     if (module_exists('libraries') && function_exists('libraries_get_libraries')) {
-      $library = libraries_get_libraries();
-      $rnjs_installed = (isset($library['responsive_navigation'])) ? TRUE : FALSE;
+      $info = libraries_info('responsive_navigation');
+      $version = $info['version'];
+      $rnjs_installed = $info['installed'];
     }
     // if Libraries module isn't installed, check manually. NOTE: This should never execute as the Libaries module is required.
     elseif (_rnjs_installed($phase)) {
@@ -56,7 +58,7 @@ function responsive_navigation_requirements($phase) {
       $rnjs_installed = FALSE;
     }
     if ($rnjs_installed) {
-      $requirements['responsive_navigation']['value'] = $t('Installed');
+      $requirements['responsive_navigation']['value'] = $version;
       $requirements['responsive_navigation']['severity'] = REQUIREMENT_OK;
     }
     else {
@@ -77,4 +79,4 @@ function _rnjs_installed($phase) {
     file_exists('sites/all/libraries/responsive_navigation/' . RNJS_MAIN_JS)
     || file_exists('profiles/' . drupal_get_profile() . '/libraries/responsive_navigation/' . RNJS_MAIN_JS)
   );
-}
\ No newline at end of file
+}
diff --git a/responsive_navigation.module b/responsive_navigation.module
index 8b7391c..7027fa6 100644
--- a/responsive_navigation.module
+++ b/responsive_navigation.module
@@ -684,6 +684,6 @@ function responsive_navigation_init() {
       );
     }
     drupal_add_js(array('responsive_navigation' => $blocks), 'setting');
+    drupal_add_library('responsive_navigation', 'responsive_navigation');
   }
-  drupal_add_library('responsive_navigation', 'responsive_navigation');
 }
