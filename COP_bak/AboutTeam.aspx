﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsForumMasterPage.master" AutoEventWireup="true" CodeFile="AboutTeam.aspx.cs" Inherits="AboutTeam" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentLeftMenu" Runat="Server">
    <div class="column left forumleft">
<h4 class="sideMenu">Community of Practice</h4>
<ul>
<li><asp:LinkButton ID="LinkButton1" PostBackUrl="~/Default.aspx" runat="server">Home</asp:LinkButton></li>
<li><asp:LinkButton ID="LinkButton2" runat="server" PostBackUrl="~/2_about.aspx">About the REMS TA Center CoP</asp:LinkButton></li>
<li><asp:Label ID="Label1" ForeColor="#85952A" runat="server"><strong>Meet the REMS TA Center Team</strong></asp:Label></li>
<li><asp:LinkButton ID="LinkButton4" runat="server" PostBackUrl="~/3_PortalRules.aspx">Community Rules</asp:LinkButton></li>
<li><asp:LinkButton ID="LinkButton3" runat="server" PostBackUrl="~/Account/Register.aspx">Join the community</asp:LinkButton></li>
<li><asp:LinkButton ID="LinkButton9" runat="server" PostBackUrl="~/REMSCOPforum/Default.aspx">Community Forums</asp:LinkButton></li>
<li><asp:LinkButton ID="lbtnloginout" PostBackUrl="~/Account/Login.aspx?ReturnUrl=../REMSCOPforum/default.aspx" runat="server" Text="Log in"></asp:LinkButton></li>
<%--<li><asp:LinkButton ID="LinkButton2" onClientClick="window.open('events.aspx');" runat="server">Events</asp:LinkButton></li>--%>
</ul>
</div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
<div class="maincolumn">
    <h1>Meet the REMS TA Center Team!</h1>
    <p>
    Thank you for being a member of the Community of Practice! Now that we’ve met you, we’d love to introduce you to our team. We are available via email and our toll-free phone line to respond to your direct requests for technical assistance on a variety of topics. From questions about registering for a webinar or scheduling a training event, to topical inquiries about new issues in emergency management—we’re here to support you!
    </p>
    <p><b><i>Bronwyn Roberts, Project Director</i></b></p>
    <p>
        Bronwyn Roberts is the Project Director for the REMS TA Center, which she has 
        supported since 2007. During this time, she has helped expand the TA Center’s 
        portfolio of resources and services to include online training courses and 
        webinars, on-site trainings by request, online tools, and a range of 
        publications for the field. Bronwyn spends her free time managing events for 
        NorCal AIDS Cycle, a nonprofit organization dedicated to raising funds to 
        support local AIDS/HIV beneficiary agencies in the northern California region. 
        She is also licensed to practice law in California, but never, ever does.
    </p>
     <p><b><i>Akshay Jakatdar, Director of Technical Assistance</i></b></p>
     <p>
     Akshay Jakatdar is the Director of Technical Assistance for the REMS TA Center. He is passionate about bridging research to practice, and sharing and learning with school and higher ed emergency management practitioners nationwide. Akshay spends his free time crafting vegetarian magic from his local Trader Joe’s and honing his ability to row a boat.
     </p>
     <p><b><i>Amanda Everett, Project Coordinator</i></b></p>
     <p>
     Amanda Everett is the REMS TA Center’s Project Coordinator. She is responsible for the seamless functioning of all activities in the TA Center, and is so versatile in her skills and abilities that she pops up in nearly every component of the TA Center’s work. In her free time, she practices her German, indulges her sweet tooth, and serves as the team’s very own style icon.
     </p>
     
	 <p><b><i>Kristin Goen, Logistics and Meetings Manager</i></b></p>
	 <p>
	 Kristin Goen is the REMS TA Center's Logistics and Meetings Manager. She is responsible for managing the TA Center's Trainings-by-Request, exhibits, and meetings. In her free time, she enjoys traveling with her young family and enjoying French cuisine!
	 </p>
      <p><b><i>Rashaad Houston, Meeting Planner/Logistics Coordinator</i></b></p>
     <p>
     Rashaad Houston is a Meeting Planner/Logistics Coordinator for the REMS TA Center. He supports us by effortlessly coordinating and managing the logistics of all Trainings by Requests, exhibitions, and other TA Center events. He has been recognized as Employee of the Quarter for providing outstanding onsite support for TA Center events. Rashaad spends his free time attending sporting events and relaxing at home with his dog, Riley and his fiancé.
     </p>
     <p><b><i>Samantha Spinney, Research Associate</i></b></p>
     <p>
     Samantha Spinney is a Research Associate at the REMS TA Center. She is passionate about the field of education (previously, she taught high school in the Fairfax County, VA, school system), and she brings a wealth of practical knowledge and experience to various projects at the TA Center. In her free time, Samantha works on her dissertation, bakes wedding cakes, and plays with her puppy, Theo.
</p>
<p><b><i>Janelle A. Williams, Communications Manager</i></b></p>
 <p>
     Janelle Williams is the REMS TA Center’s Communications Manager. She ensures that all TA Center work meets the U.S. Department of Education’s standards by writing brilliant copy and providing exceptional management of multimedia and marketing projects. She is constantly innovating effective ways to reach the field through creative marketing techniques. She spends her free time pursuing many hobbies, including creative writing and interior design.
     </p>
<p>
We always welcome your input on the forums, web chats, and resources available. If you have suggestions for additional resources you would find helpful here, feel free to upload them to our <a href="http://rems.ed.gov/ResourceSubmission/ResourceSubmissions.aspx" target="_blank">Tool Box.</a></p><p><a href="mailto:info@remstacenter.org">Drop us a note</a> to share your feedback, or to let us know about a high-quality plan or plan component, novel or innovative practice, or a story of lessons learned from an actual emergency you would like to contribute to the field.
     </p>
</div>
</asp:Content>
<%--<asp:Content ID="Content4" ContentPlaceHolderID="ContentRightMenu" Runat="Server">
</asp:Content>--%>

