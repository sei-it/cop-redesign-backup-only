﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsForumMasterPage.master" AutoEventWireup="true" CodeFile="2_About.aspx.cs" Inherits="_2_About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentLeftMenu" Runat="Server">
    <div class="column left forumleft">
<h4 class="sideMenu">Community of Practice</h4>
<ul>
<li><asp:LinkButton ID="LinkButton1" PostBackUrl="~/Default.aspx" runat="server">Home</asp:LinkButton></li>
<li><asp:Label ID="lblid2" ForeColor="#85952A" runat="server"><strong>About the REMS TA Center CoP</strong></asp:Label></li>
<%--<li><asp:LinkButton ID="LinkButton2" runat="server" PostBackUrl="~/AboutTeam.aspx">Meet the REMS TA Center Team</asp:LinkButton></li>--%>
<li><asp:LinkButton ID="LinkButton4" runat="server" PostBackUrl="~/3_PortalRules.aspx">Community Rules</asp:LinkButton></li>
<li><asp:LinkButton ID="LinkButton3" runat="server" PostBackUrl="~/Account/Register.aspx">join the community</asp:LinkButton></li>
<li><asp:LinkButton ID="lbtnCOPindex" runat="server" Visible="false" PostBackUrl="~/REMSCOPforum/COPindex.aspx">Community Forums</asp:LinkButton></li>
<li><asp:LinkButton ID="lbtnloginout" PostBackUrl="~/Account/Login.aspx?ReturnUrl=../REMSCOPforum/COPindex.aspx" runat="server" Text="Log in"></asp:LinkButton></li>
<%--<li><asp:LinkButton ID="LinkButton2" onClientClick="window.open('events.aspx');" runat="server">Events</asp:LinkButton></li>--%>

</ul>
</div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
    <%--<div class="main3x">--%>
    <div class="main2x">
    <h1>
       About the REMS TA Center Community of Practice
    </h1>
    <p>
The REMS TA Center Community of Practice (CoP) is a virtual space for representatives from schools, school districts, institutions of higher education (IHEs), and their community partners to collaborate on special projects, share news and resources, discuss trends and ideas, and learn from the experiences of others in the field. 
    </p>
    <ul>
        <li><b>What is the purpose?</b> To create a collaborative community that enhances the ability of schools, IHEs, and their community partners to develop high-quality emergency operations plans (EOPs) and implement comprehensive emergency management planning efforts.  </li>
        <li><b>Who should join? </b>Anyone working in the field of school and/or IHE emergency management, from members of emergency operations planning teams and law enforcement officers to community members, and parents.</li>
        <li>
            <b>Are there any requirements to join?</b> Yes. We value respectful and constructive collaboration. <a href="3_PortalRules.aspx" >View our Community Rules.</a>
        </li>
    </ul>
    <p>
        All REMS TA Center CoP members can collaborate and interact via our topic-based forums. Explore our <a href="REMSCOPforum/default.aspx">forums</a> to learn more.
</p>
    <p>
After joining, members can start groups to facilitate more focused discussions, collaborate on special projects, or streamline access to local content and conversations. 
    </p>
    <p>Explore our <a href="Account/Login.aspx">groups</a> to learn more.</p>
</div>


<div class="columnSmall right" runat="server" visible="false"> 
				<div id="newsRotator" class="featureBox">
                    <a href="#" onclick="return false;" class="control_next">&nbsp;</a>
                    <a href="#" onclick="return false;" class="control_prev">&nbsp;</a>
						<h3 class="featureHead icon_cal">
							<%--News &amp; Highlights</h3>--%>
                          <a href="NewsandHighlights.aspx">News &amp; Highlights</a></h3>
					  <ul>
						
                         <li>
                        <img src="../../images/humanTrafficking.jpg" alt="Human Trafficking 101 for School Administrators and Staff" />
                        <h3>Human Trafficking 101 for School Administrators and Staff</h3>
                         <p><a href="docs/Human Trafficking 101 for School Administrators and Staff.pdf" target="_blank">Download the Report</a></p>
                      </li>

                        <li>
                        <img src="../../images/colleagueLetter.jpg" alt="Man reading letter on tablet device" />
                        <h3><a href="http://www.ifap.ed.gov/dpcletters/GEN1413.html" target="_blank">Guidance to Institutions on Compliance with Amended Clery Act</a></h3>
                        <p><a href="http://www.ifap.ed.gov/dpcletters/GEN1413.html" target="_blank">Download the Dear Colleague Letter</a></p>
                      </li>
                      
                      <li>
                        <img src="../../images/confrontingExploitation.jpg" alt="Screen capture from promotional video" />
                        <h3><a href="http://www.iom.edu/Reports/2013/Confronting-Commercial-Sexual-Exploitation-and-Sex-Trafficking-of-Minors-in-the-United-States.aspx" target="_blank">Confronting Commercial Sexual Exploitation and Sex Trafficking of Minors in the U.S.</a></h3>
                        <p><a href="http://www.iom.edu/Reports/2013/Confronting-Commercial-Sexual-Exploitation-and-Sex-Trafficking-of-Minors-in-the-United-States.aspx" target="_blank">Download Report Here</a>​</p>
                      </li>
                      
						<li>
                        <img src="../../images/collegeSafety-1.jpg" alt="College students in common area of campus" />
                        <h3>Keeping Campuses Safe</h3>
                        <p>U.S. Department of Education Announces Action to <a href="http://www.ed.gov/news/press-releases/us-department-education-announces-action-help-colleges-keep-campuses-safe" target="_blank">Help Colleges Keep Campuses Safe</a></p>
                      </li>
                        
						<li>
                        	<img src="../../images/schoolCrime2013.gif" alt="2013 School Crime and Saftey Report" >
                            <h3><a href="http://www.bjs.gov/content/pub/pdf/iscs13.pdf" target="_blank">New 2013 School Crime and Safety Report Released</a></h3>
                            <p><a href="http://www.bjs.gov/content/pub/pdf/iscs13.pdf" target="_blank">Download the Full Report</a></p>
                      </li>
                        
						<li>
                        <img src="../../images/vftf-banner.gif" alt="Voices from the Field" >
                        <h3><a href="http://safesupportivelearning.ed.gov/voices-field/which-activity-most-helpful-gauging-student-perception-your-schooldistrictcommunity" target="_blank">Share Your Voice from the Field on Student Perception</a></h3>
                        <p><a href="http://safesupportivelearning.ed.gov/voices-field/which-activity-most-helpful-gauging-student-perception-your-schooldistrictcommunity" target="_blank">Respond to a Survey and Share Experiences</a></p>
                      </li>

                      <li>
                      <img src="../../images/PreventingYouthViolence.jpg" alt="Preventing Youth Violence banner" >
                      <h3><a href="http://www.cdc.gov/violenceprevention/youthviolence/Opportunities-for-Action.html" target="_blank">HHS/CDC Releases Preventing Youth Violence: Opportunities for Action </a></h3>
                      <p><a href="http://www.cdc.gov/violenceprevention/youthviolence/Opportunities-for-Action.html" target="_blank">Download the Full Report</a></p>
                      </li>
        

					</ul>

                    <div class="slider_option">
                      <input type="checkbox" id="checkbox" checked="checked" />
                      <label for="checkbox">Autoplay Slider</label>
                    </div> 
                    
                    
				</div>
  

                   <%--  <h3>Leadership Meeting on School Safety, Security, and Emergency Management</h3>
                     <p><a href="leadershipmtg.aspx">Materials Available Here</a></p>--%>


        
              
                 
                 
             
                 
                   
<div class="featureBox">
						<h3 class="featureHead icon_page">
							Quick Links</h3>

                             <%--<asp:Literal ID="LiteralQuickLinks" runat="server"></asp:Literal>		--%>	           
                    <ul>
                        <li><a href="TA_TrainingsByRequest.aspx">Request a Training</a></li> 
                        <li><a href="RequestTA.aspx">Request TA</a></li> 
                        <li><a href="TA_VirtualTrainings.aspx">Take a Webinar</a></li>
                        <li><a href="Resources.aspx">Find a Resource</a></li>
                        <li><a href="stateresources.aspx">Access State Emergency Management Resources</a></li>
                        <li><a href="granteecorner.aspx">Grantee Corner</a></li>
                        <li><a href="banners.aspx">Download Creative Materials</a></li>
					</ul>
                             		
				</div>
    </div> <!-- / column right -->

</asp:Content>

