﻿<%@ Page Title="Log In" Language="C#" MasterPageFile ="~/remsForumMasterPage.master" AutoEventWireup="true"
    CodeFile="Login.aspx.cs" Inherits="Account_Login" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentMain">
<div class="maincolumn" style="text-align:center;" >
<asp:Panel ID="LoginView" runat="server" >
    <h2>
        Log In
    </h2>
    <p>
        Please enter your username and password.
        <asp:HyperLink ID="RegisterHyperLink" runat="server" NavigateUrl="Register.aspx" EnableViewState="false">Register</asp:HyperLink> if you don't have an account.
    </p>
    <asp:Login ID="LoginUser" runat="server" EnableViewState="false" RenderOuterTable="false">
        <LayoutTemplate>
            <span class="failureNotification">
                <asp:Literal ID="FailureText" runat="server"></asp:Literal>
            </span>
            <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" CssClass="failureNotification" 
                 ValidationGroup="LoginUserValidationGroup"/>
            <div class="accountInfo">
                <fieldset class="login">
                    <legend>Account Information</legend>
                    <p>
                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Username:</asp:Label>
                        <asp:TextBox ID="UserName" runat="server" Text="" EnableViewState="false" CssClass="textEntry"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" 
                             CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="User Name is required." 
                             ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                    </p>
                    <p>
                        <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                        &nbsp;<asp:TextBox ID="Password" runat="server" CssClass="passwordEntry" Text="" EnableViewState="false" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" 
                             CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required." 
                             ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                    </p>
                    <p>
                        <asp:CheckBox ID="RememberMe" runat="server" Checked="false" />
                        <asp:Label ID="RememberMeLabel" runat="server" AssociatedControlID="RememberMe"  CssClass="inline">Keep me logged in</asp:Label>
                    </p>
                </fieldset>
                <p class="submitButton">
                    <asp:Button ID="LoginButton" class="btnCop" runat="server" CommandName="Login" Text="Log In" ValidationGroup="LoginUserValidationGroup"/>
                <asp:Button ID="btnLostPassword" class="btnCop"  runat="server" Text="Forgot Password" OnClick="btnLostPassword_Click" />
                <asp:Button ID="Button2" class="btnCop" runat="server" Text="Change Password" OnClick="btnChangePassword_Click" />
                </p>
            </div>
        </LayoutTemplate>
    </asp:Login>
</asp:Panel>
<asp:Panel ID="RecoverView" runat="server" Visible="false" DefaultButton="Recover">
            <h2>
                Get Temporary Password
            </h2>
            <p>
                Enter your username below to have a temporary password sent to your email.</p>
            <div>
                <table border="0" cellpadding="0" >
                    <tr>
                        <td align="left">
                        Username:&nbsp; 
                            </td>
                        <td>
                           <asp:TextBox  ID="LostUserName" runat="server" Text="" Width="300"/>*
                        </td>
                    </tr>
                     <tr>
                        <td align="left">
                            Email:&nbsp;</td>
                        <td>
                            <asp:TextBox  ID="txtEmail" runat="server" Width="300" Text="" />*
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                            <asp:Button ID="Recover" runat="server" class="btnCop" Text="Get temporary password"
                                CausesValidation="false" OnClick="Recover_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
<asp:Panel ID="ResetView" runat="server" Visible="false" DefaultButton="button1">
            <h2>
                Change Password</h2>
            <p>
                Your new password must be at least 6 characters.</p>
            <table >
                <tr>
                    <td align="left">
                        Username:&nbsp;
                    </td>
                    <td>
                        <asp:TextBox class="msapTxt" ID="txtResetUserName" runat="server" Text="" />*
                        <asp:RequiredFieldValidator Font-Size="Smaller" ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtResetUserName"
                            ErrorMessage="This filed is required!"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Old Password:
                    </td>
                    <td>
                        <asp:TextBox ID="txtOldPassword" runat="server" class="msapTxt" Text="" TextMode="Password"></asp:TextBox>*
                        <asp:RequiredFieldValidator Font-Size="Smaller" ID="RequiredFieldValidator" runat="server" ControlToValidate="txtOldPassword"
                            ErrorMessage="This filed is required!"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        New Password:
                    </td>
                    <td>
                        <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" Text=""></asp:TextBox>*
                        <span style="font-size:smaller;"><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNewPassword"
                                ErrorMessage="This filed is required!"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Your new password must be at least 6 characters."
                                ControlToValidate="txtNewPassword" ValidationExpression="[0-9a-zA-Z!@#$%^&*()]{6,}" />
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        Repeat Password:
                    </td>
                    <td>
                        <asp:TextBox ID="txtRepeatPassword" runat="server" class="msapTxt" TextMode="Password" Text=""></asp:TextBox>*
                        <span style="font-size:smaller;" ><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtRepeatPassword"
                            ErrorMessage="This filed is required!"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtNewPassword"
                            ControlToValidate="txtRepeatPassword" ErrorMessage="Please re-type your new password!"></asp:CompareValidator>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="left">
                        <asp:Button ID="button1" runat="server" class="btnCop" Text="Change Password" CausesValidation="true"
                            OnClick="OnChangePassword" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
<p style="color: red;">
            <asp:Label class="etacTxt" ID="lblError" runat="server" Visible="false" />
        </p>
</div>
</asp:Content>
