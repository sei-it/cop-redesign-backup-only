﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Text;
using System.Net;

public partial class Account_Register : System.Web.UI.Page
{
    remsUsersDataClassesDataContext db = new remsUsersDataClassesDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        RegisterUser.ContinueDestinationPageUrl = Request.QueryString["ReturnUrl"];
        if (Session["aspnetforumUserID"] == null)
        {
            lbtnloginout.Text = "Log In";
            lbtnCOPindex.Visible = false;
        }
        else
        {
            lbtnloginout.Text = "Log Out";
            lbtnCOPindex.Visible = true;
            
        }
        //if (!IsPostBack)
        //{
        //    initialAllTextFields(true);
        //}
    }
    protected void SetRoles(Object sender, EventArgs e)
    {
        foreach(ListItem item in cblInstType.Items)
        {
            if (item.Value == "K-12" && !item.Selected)
            {
                rblK12Role.Enabled = false;
                foreach (ListItem elmt in rblK12Role.Items)
                    elmt.Selected = false;
            }
            else if (item.Value == "K-12" && item.Selected)
                rblK12Role.Enabled = true;
            else if (item.Value == "Higher ed" && !item.Selected)
            {
                rblHigherRole.Enabled = false;
                foreach (ListItem elmt in rblHigherRole.Items)
                    elmt.Selected = false;
            }
            else if (item.Value == "Higher ed" && item.Selected)
                rblHigherRole.Enabled = true;
        }
        if (cblInstType.Items.Count == 0)
        {
            rblHigherRole.Enabled = false;
            rblK12Role.Enabled = false;
        }
    }

    protected void RegisterUser_CreatedUser(object sender, EventArgs e)
    {

        FormsAuthentication.SetAuthCookie(RegisterUser.UserName, false /* createPersistentCookie */);

        string continueUrl = RegisterUser.ContinueDestinationPageUrl;
        if (String.IsNullOrEmpty(continueUrl))
        {
            continueUrl = "~/";
        }

        //pass user's First/Last names and email to the forum application
        HttpContext.Current.Session["Email"] = txtEmail.Text.Trim();
        HttpContext.Current.Session["FirstName"] = txtFirstName.Text.Trim();
        HttpContext.Current.Session["LastName"] = txtLastName.Text.Trim();

        //create a user profile.

        remsUser user = initialAllTextFields(false);
        db.remsUsers.InsertOnSubmit(user);
        db.SubmitChanges();
        SendMailErrorEventArgs(txtEmail.Text.Trim(), txtFirstName.Text.Trim() + " " + txtLastName.Text.Trim());

        initialAllTextFields(true);

        //Response.Redirect(continueUrl);
    }

    private void SendMailErrorEventArgs(string email, string name)
    {
        string strTo = email;
        string strFrom = "info@remstacenter.org";
        MailMessage objMailMsg = new MailMessage(strFrom, strTo);

        TextBox username = (TextBox)RegisterUserWizardStep.ContentTemplateContainer.FindControl("UserName");
        TextBox passwrod = (TextBox)RegisterUserWizardStep.ContentTemplateContainer.FindControl("Password");

        objMailMsg.CC.Add("info@remstacenter.org");
        
        //string copBody = "Dear " + name + ",<br/><br/>";
        //copBody += " Thank you for your joining the community. ";
        objMailMsg.BodyEncoding = Encoding.UTF8;
        objMailMsg.Subject = "Thank you for Joining the Community";
        //objMailMsg.Body = copBody;
        objMailMsg.Body = "<p><b>Thank you for registering for the Readiness and Emergency Management for Schools (REMS) Technical Assistance (TA) Center Community of Practice. Please use the information below to login to your account:</b></p>";
        objMailMsg.Body += "<p>User Name (Email Address): " + username.Text + "<br/>";
        objMailMsg.Body += "Password: " + passwrod.Text + "</p>";
        objMailMsg.Body += "<b><p>If you require technical support accessing the Community of Practice, please email <a href='mailto:info@remstacenter.org'>info@remstacenter.org</a>.</p><p>Sincerely,<br/>The REMS TA Center Team</p></b>";
        objMailMsg.Priority = MailPriority.High;
        objMailMsg.IsBodyHtml = true;

        //prepare to send mail via SMTP transport
        SmtpClient objSMTPClient = new SmtpClient();
        objSMTPClient.Host = "mail2.seiservices.com";
        NetworkCredential userCredential = new NetworkCredential("SEInfo@seiservices.com", "");
        objSMTPClient.Credentials = CredentialCache.DefaultNetworkCredentials;
        try
        {
            objSMTPClient.Send(objMailMsg);
        }
        catch (Exception exc)
        {
            Response.Write("Send failure: " + exc.ToString());
        }

       // ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Thank you for submitting your data. ');</script>", false);

    }

    private remsUser initialAllTextFields(bool isClearout)
    {
        remsUser user = new remsUser();
        string selectedItmesholder = "";
        if (isClearout)
        {
            foreach (ListItem item in cblInstType.Items)
            {
                item.Selected = false;
            }

            foreach (ListItem item in rblK12Role.Items)
            {
                item.Selected = false;
            }

            foreach (ListItem item in rblHigherRole.Items)
            {
                item.Selected = false;
            }

            txtOtherRoleDes.Text = "";
            txtFirstName.Text = "";
            txtLastName.Text = "";
            ddlState.SelectedIndex = 0;
            txtCity.Text = "";
            txtEmail.Text = "";
            txtyrExpED.Text = "";
            txtyrExpEmergency.Text = "";

            foreach (ListItem item in cblInterests.Items)
            {
                item.Selected = false;
            }

            foreach (ListItem item in rblAccept.Items)
            {
                item.Selected = false;
            }

            

        }
        else  //inital remsUser object
        {
            selectedItmesholder = "";
            foreach (ListItem item in cblInstType.Items)
            {
                if (item.Selected)
                {
                    selectedItmesholder += string.IsNullOrEmpty(selectedItmesholder) ? item.Value.Trim() : "," + item.Value.Trim();
                }
            }
            user.institutionType = selectedItmesholder;

            selectedItmesholder = "";
            foreach (ListItem item in rblK12Role.Items)
            {
                if (item.Selected)
                {
                    selectedItmesholder += string.IsNullOrEmpty(selectedItmesholder) ? item.Text.Trim() : "," + item.Text.Trim();
                }
            }
            user.k12Role = selectedItmesholder;

            selectedItmesholder = "";
            foreach (ListItem item in rblHigherRole.Items)
            {
                if (item.Selected)
                {
                    selectedItmesholder += string.IsNullOrEmpty(selectedItmesholder) ? item.Text.Trim() : "," + item.Text.Trim();
                }
            }
            user.higherEDRole = selectedItmesholder;

            user.otherRoleDes = txtOtherRoleDes.Text.Trim();
            //txtFirstName.Text.Trim();
            user.FirstName = txtFirstName.Text.Trim();
            user.LastName = txtLastName.Text.Trim();
            user.State = ddlState.SelectedItem.Text.Trim();
            user.City = txtCity.Text.Trim();
            user.Email = txtEmail.Text.Trim();
            user.yrExpED = Convert.ToInt32(txtyrExpED.Text.Trim());
            user.yrExpEmergency = Convert.ToInt32(txtyrExpEmergency.Text.Trim());
            user.SelectedType = txtSelectedTypes.Text.Trim();

            selectedItmesholder = "";
            foreach (ListItem item in cblInterests.Items)
            {
                if (item.Selected)
                {
                    selectedItmesholder += string.IsNullOrEmpty(selectedItmesholder) ? item.Text.Trim() : "," + item.Text.Trim();
                }
            }
            user.interests = selectedItmesholder;


            foreach (ListItem item in rblAccept.Items)
            {
                if (item.Value == "1" && item.Selected)
                    user.isAccept = item.Selected;
            }
            user.aspnet_UserName = ((TextBox)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("UserName")).Text.Trim();

            MembershipUser User = Membership.GetUser(user.aspnet_UserName);

            object UserGUID = User.ProviderUserKey;

            user.Userid = (Guid)UserGUID;
        }

        user.CreatedOn = DateTime.Now;

        return user;
    }

    protected void ContinueButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("login.aspx");
    }
    protected void SaveData(object sender, EventArgs e)
    {
        TextBox txtnewUserEmail = ((TextBox)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("Email"));
        txtnewUserEmail.Text = txtEmail.Text.Trim();
    }
    //register form page properties
class LoginUserInfo
{
    private string _aspnet_UserName;
    public string Aspnet_UserName
    {
        get { return this._aspnet_UserName;}
        set { _aspnet_UserName =value;}
    }

    private string _institutionType;
    public string InstitutionType
    {
        get {return this._institutionType  ;}
        set{ this._institutionType = value;}
    }

    private string _k12Role;
    public string K12Role
    {
        get {return this._k12Role ;}
        set{ this._k12Role = value;}
    }

    private string _higherEDRole;
    public string HigherEDRole
    {
        get {return this._higherEDRole ;}
        set{ this._higherEDRole = value;}
    }

    private string _otherRoleDes;
    public string OtherRoleDes
    {
        get {return this._otherRoleDes ;}
        set{ this._otherRoleDes = value;}
    }

    private string _firstname;
    public string FirstName
    {
        get {return this._firstname ;}
        set{ this._firstname = value;}
    }

    private string _lastname;
    public string LastName
    {
        get {return this._lastname ;}
        set{ this._lastname = value;}
    }

    private string _state;
    public string State
    {
        get {return this._state ;}
        set{ this._state = value;}
    }

    private string _city;
    public string City
    {
        get {return this._city ;}
        set{ this._city = value;}
    }

    private string _email;
    public string Email
    {
        get {return this._email ;}
        set{ this._email = value;}
    }

    private string _selectedtype;
    public string SelectedType
    {
        get {return this._selectedtype ;}
        set{ this._selectedtype = value;}
    }

    private int _yrexpemergency;
    public int yrExpEmergency
    {
        get {return this._yrexpemergency ;}
        set{ this._yrexpemergency = value;}
    }

      private int _yrexpED;
    public int yrExpED
    {
        get {return this._yrexpED ;}
        set{ this._yrexpED = value;}
    }

     private string _interests;
    public string Interests
    {
        get {return this._interests ;}
        set{ this._interests = value;}
    }

    private bool _isaccept;
    public bool IsAccept
    {
        get{return this._isaccept;}
        set { this._isaccept = value; }

}


}


}
