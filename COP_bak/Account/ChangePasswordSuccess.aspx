﻿<%@ Page Title="Change Password" Language="C#" MasterPageFile="~/remsForumMasterPage.master" AutoEventWireup="true"
    CodeFile="ChangePasswordSuccess.aspx.cs" Inherits="Account_ChangePasswordSuccess" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="leftContent" runat="server" ContentPlaceHolderID="ContentLeftMenu">
<div class="forumSideNav">
    <div class="column left">
<table>
<tr>
<td>
    <asp:LinkButton ID="LinkButton1" PostBackUrl="~/Default.aspx" runat="server">Home</asp:LinkButton></td>
</tr>
<tr>
<td><asp:LinkButton ID="LinkButton2" runat="server" PostBackUrl="~/2_About.aspx">About the REMS TA Center CoP</asp:LinkButton></td>
</tr>
<tr>
<%--<td><asp:LinkButton ID="LinkButton7" runat="server" PostBackUrl="~/AboutTeam.aspx">Meet the REMS TA Center Team</asp:LinkButton></td>--%>
</tr>
<tr>
<td><asp:LinkButton ID="LinkButton4" Width="200" runat="server" PostBackUrl="~/3_PortalRules.aspx">Community Rules</asp:LinkButton></td>
</tr>
<tr>
<td><asp:LinkButton ID="LinkButton6" runat="server" PostBackUrl="~/Account/Register.aspx">join the community</asp:LinkButton></td>
</tr>
<tr>
<td><asp:Label ID="LinkButton3" runat="server" ForeColor="White" BackColor="#2E9AFE"><b>Log In</b></asp:Label></td>
</tr>
<tr>
<td>
<%--<asp:LinkButton ID="LinkButton5" onClientClick="window.open('../events.aspx');" runat="server">Events</asp:LinkButton>--%>
</td>
</tr>
</table>
</div>
   </div> <!-- /ForumSideNav -->
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentMain">
    <h2>
        Change Password
    </h2>
    <p>
        Your password has been changed successfully.
    </p>
</asp:Content>
