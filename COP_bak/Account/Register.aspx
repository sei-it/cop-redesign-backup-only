﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/remsForumMasterPage.master" AutoEventWireup="true"
    CodeFile="Register.aspx.cs" Inherits="Account_Register" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <script language="javascript" type="text/javascript">
    function ValidateChkList(source, arguments) {
        var validateID = source.id.toString().split('_')[3];
        arguments.IsValid = IsCheckBoxChecked(validateID) ? true : false;
        if (validateID == "CustomValidator2" || validateID == "CustomValidator3") {
            var otherDes = $("#<%= txtOtherRoleDes.ClientID %>").val();

            if (otherDes.trim() != "")
                arguments.IsValid = true;

        }
    }

  
    function IsCheckBoxChecked(vid) {
        var isChecked = false;
        var list = null;
        var typelist = document.getElementById('<%= cblInstType.ClientID %>');
        if (vid == "CustomValidator1")
            list = typelist;
        else if (vid == "CustomValidator2")
            list = document.getElementById('<%= rblK12Role.ClientID %>');
        else if (vid == "CustomValidator3")
            list = document.getElementById('<%= rblHigherRole.ClientID %>');
        else if (vid == "CustomValidator4")
            list = document.getElementById('<%= cblInterests.ClientID %>');
        else if (vid == "CustomValidator5") {
            var accpt = document.getElementById('ctl00_ContentMain_RegisterUser_rblAccept_0');
            if (accpt.checked == true)
                isChecked = true;
        }
        
        if (list != null) {
            for (var i = 0; i < list.rows.length; i++) {
                for (var j = 0; j < list.rows[i].cells.length; j++) {
                    var listControl = list.rows[i].cells[j].childNodes[0];
                    if (listControl!=null && listControl.checked) {
                        if (vid == "CustomValidator1" || "CustomValidator4")
                            isChecked = true;
                        else if(vid == "CustomValidator2" && typelist.rows[0].cells[0].childNodes[0].checked)
                            isChecked = true;
                        else if (vid == "CustomValidator3" && typelist.rows[0].cells[1].childNodes[0].checked)
                            isChecked = true;
                        else if (vid == "CustomValidator5" && typelist.rows[0].cells[0].childNodes[0].checked)
                            isChecked = true;
                    }
                }
            }
        }

        if (vid == "CustomValidator2" && !typelist.rows[0].cells[0].childNodes[0].checked)
            isChecked = true;
        else if (vid == "CustomValidator3" && !typelist.rows[0].cells[1].childNodes[0].checked)
            isChecked = true;

        return isChecked;
    }


    </script>
     <style type="text/css">
    .failureNotification
    { 
      color:Red;
    }
    
    </style>
</asp:Content>
<asp:Content ID="leftContent" runat="server" ContentPlaceHolderID="ContentLeftMenu">

    <div class="column left forumleft">
<h4 class="sideMenu" >Community of Practice</h4>
<ul >
<li><asp:LinkButton ID="LinkButton1" PostBackUrl="~/Default.aspx" runat="server" CausesValidation="false">Home</asp:LinkButton></li>
<li><asp:LinkButton ID="LinkButton2" PostBackUrl="~/2_About.aspx" runat="server" CausesValidation="false">About the REMS TA Center CoP</asp:LinkButton></li>
<%--<li><asp:LinkButton ID="LinkButton3" runat="server" PostBackUrl="~/AboutTeam.aspx" CausesValidation="false">Meet the REMS TA Center Team</asp:LinkButton></li>--%>
<li><asp:LinkButton ID="LinkButton4" PostBackUrl="~/3_PortalRules.aspx" runat="server" CausesValidation="false">COMMUNITY RULES</asp:LinkButton></li>
<li><asp:Label ID="lblid2" ForeColor="#85952A" runat="server"><strong>join the community</strong></asp:Label></li>
<li><asp:LinkButton ID="lbtnCOPindex" runat="server" Visible="false" PostBackUrl="~/REMSCOPforum/COPindex.aspx" CausesValidation="false">Community Forums</asp:LinkButton></li>
<li><asp:LinkButton ID="lbtnloginout" CausesValidation="false" PostBackUrl="~/Account/Login.aspx?ReturnUrl=../REMSCOPforum/COPindex.aspx" runat="server" Text="Log in"></asp:LinkButton></li>
<%--<li><asp:LinkButton ID="LinkButton5" onClientClick="window.open('../events.aspx');" runat="server" CausesValidation="false">Events</asp:LinkButton></li>--%>
</ul>
</div>
  
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentMain">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
<div class="maincolumn">
    <%--Create a new user--%>
    <asp:CreateUserWizard ID="RegisterUser" runat="server" EnableViewState="true" 
        OnNextButtonClick="SaveData"  OnCreatedUser="RegisterUser_CreatedUser">
        <LayoutTemplate>
            <asp:PlaceHolder ID="wizardStepPlaceholder" runat="server"></asp:PlaceHolder>
            <asp:PlaceHolder ID="navigationPlaceholder" runat="server"></asp:PlaceHolder>
        </LayoutTemplate>
        <WizardSteps>
        <asp:WizardStep ID="EnrollmentStep" runat="server" >

        <h1>Join Community Form</h1>
    <p>Thank you for joining the REMS TA Center Community of Practice (CoP), where you can collaborate on special projects, share news and resources, discuss trends and ideas, and learn from the experiences of others in the field. By joining this CoP, you will also have automatic access to our Virtual Training Courses. Visit the REMS TA Center Training page after you’ve completed your profile to learn more.</p>

    <p>We want to know more about you and your work. Please confirm which institution type you represent.</p>
    <p>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        <asp:CheckBoxList ID="cblInstType" runat="server" OnSelectedIndexChanged="SetRoles" AutoPostBack="true" RepeatColumns="3" >
            <asp:ListItem Value="K-12"><strong>K-12</strong></asp:ListItem>
            <asp:ListItem Value="Higher ed"><strong>Higher ed</strong></asp:ListItem>
            <asp:ListItem Value="Government Agency or Community Partner"><strong>Government Agency or Community Partner</strong></asp:ListItem>
        </asp:CheckBoxList>
        
        
         <asp:CustomValidator ID="CustomValidator1" ClientValidationFunction="ValidateChkList"
            runat="server" >An institution type is required.</asp:CustomValidator>
    </p>
    <p>
     Which of the following best describes your role? 

    </p>
 
    <div style="clear:right">
        <div style="width:300px;float:left;">
            <h5>K-12</h5>
            <asp:RadioButtonList ID="rblK12Role" runat="server" Enabled="false">
                <asp:ListItem Value="1">K-12 Teacher or Administrator</asp:ListItem>
                <asp:ListItem Value="2">Law Enforcement Officer or School Cop</asp:ListItem>
                <asp:ListItem Value="3">School Resource Officer</asp:ListItem>
                <asp:ListItem Value="4">EMS Practitioner</asp:ListItem>
                <asp:ListItem Value="5">Community Member</asp:ListItem>
                <asp:ListItem Value="6">Afterschool Services Provider</asp:ListItem>
                <asp:ListItem Value="7">Facilities Manager</asp:ListItem>
                <asp:ListItem Value="8">Parent</asp:ListItem>
            </asp:RadioButtonList>
            <asp:CustomValidator ID="CustomValidator2" ClientValidationFunction="ValidateChkList" 
            runat="server" >A K-12 role is required.</asp:CustomValidator>

        </div>
         <div style="width:330px;float:left;">
             <h5>Higher   ed</h5>
             <asp:RadioButtonList ID="rblHigherRole" runat="server"  Enabled="false">
                 <asp:ListItem Value="1">IHE Faculty, Staff, or Administrator</asp:ListItem>
                 <asp:ListItem Value="2">Law Enforcement Officer or Campus Cop</asp:ListItem>
                 <asp:ListItem Value="3">EMS Practitioner</asp:ListItem>
                 <asp:ListItem Value="4">Community Member</asp:ListItem>
                 <asp:ListItem Value="5">Campus or Building Director</asp:ListItem>
                 <asp:ListItem Value ="6">Facilities and Operations Director</asp:ListItem>
                 <asp:ListItem Value="7">Parent</asp:ListItem>
             </asp:RadioButtonList>
              <asp:CustomValidator ID="CustomValidator3" ClientValidationFunction="ValidateChkList" 
            runat="server" >A higher ed role is required.</asp:CustomValidator>
         </div>
    </div>
    </ContentTemplate>
        </asp:UpdatePanel>
    <p style="clear:both;">
        Don’t see your role listed? Type it in the field below. 
    </p>
    <p>
        <asp:TextBox ID="txtOtherRoleDes" TextMode="MultiLine" runat="server" Height="41px" Width="640px"></asp:TextBox>
    </p>
    <p>
        Do you want to stay connected? 
    </p>
    <p>
       Last, First Name: <br />
        <asp:TextBox ID="txtLastName" runat="server" Width="250"></asp:TextBox><span class="reqred">*</span> 
        <asp:TextBox ID="txtFirstName" runat="server" Width="250"></asp:TextBox><span class="reqred">*</span>
        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtLastName"
                                     ErrorMessage="Last name is required." />
        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtFirstName"
                                     ErrorMessage="First name is required." />
    </p>
    <p>
        State, City: <br /><asp:DropdownList ID="ddlState" DataSourceID="dsState" runat="server" 
                           DataTextField="Location" DataValueField="StateId" Width="250">
        </asp:DropdownList>,<span class="reqred">*</span> <asp:TextBox ID="txtCity" runat="server" Width="250"></asp:TextBox><span class="reqred">*</span>
        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" InitialValue="1" Display="Dynamic" ControlToValidate="ddlState"
                                     ErrorMessage="State is required." ></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="txtCity"
        ErrorMessage="City is required." />
    </p>
    <p>
        Email Address:<br /><asp:TextBox ID="txtEmail" runat="server" Width="500"></asp:TextBox><span class="reqred">*</span>
        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="txtEmail"
        ErrorMessage="Email is required." />

    </p>
    <p>
        School/District OR Higher ed/Campus Community Represented: 
   <br /><asp:TextBox ID="txtSelectedTypes" runat="server" Width="250"></asp:TextBox> <span class="reqred">*</span>
    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ControlToValidate="txtSelectedTypes"
        ErrorMessage="Selected type is required." />
    </p>
    <p>
       How many years of experience do you have in the field of emergency management?<br />
<asp:TextBox ID="txtyrExpEmergency" runat="server"></asp:TextBox><span class="reqred">*</span>
<ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                TargetControlID="txtyrExpEmergency"></ajax:FilteredTextBoxExtender>
<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="txtyrExpEmergency"
        ErrorMessage="Years of experience in EM are required." />
        </p>
    <p>
How many years of experience do you have in the field of education?<br />
<asp:TextBox ID="txtyrExpED" runat="server"></asp:TextBox><span class="reqred">*</span>
<ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                TargetControlID="txtyrExpED">
                            </ajax:FilteredTextBoxExtender>
<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator9" ControlToValidate="txtyrExpED"
        ErrorMessage="Years of experience in ED are required." />
</p>
    <p>
        I am interested in: (User to select from the following options.)
    </p>
    <p>
        <asp:CheckBoxList ID="cblInterests" runat="server"  RepeatColumns="2">
            <asp:ListItem>Taking a Virtual Training Course</asp:ListItem>
            <asp:ListItem>Telling a Story</asp:ListItem>
            <asp:ListItem>Sharing an Experience and/or Content</asp:ListItem>
            <asp:ListItem>Accessing Resources </asp:ListItem>
            <asp:ListItem>Exploring Lessons Learned</asp:ListItem>
            <asp:ListItem>Asking Questions</asp:ListItem>
            <asp:ListItem>Providing Support </asp:ListItem>
            <asp:ListItem>Getting Support</asp:ListItem>
            <asp:ListItem>Collaborating with Others in the Field</asp:ListItem>
            <asp:ListItem>Posting Resources for Discussion</asp:ListItem>
            <asp:ListItem>Becoming a Community Steward</asp:ListItem>
        </asp:CheckBoxList>
<asp:CustomValidator ID="CustomValidator4" ClientValidationFunction="ValidateChkList"
            runat="server" >Your interested options are required.</asp:CustomValidator>
    </p>
    <p>To view and accept the REMS TA Center CoP Codes of Conduct and Terms of Use click <a href="../3_PortalRules.aspx" target="_blank">here </a> </p>
    <p><span class="reqred">*</span> - Required Field</p>
 
        </asp:WizardStep>
        <asp:WizardStep ID="RuleStep" runat="server">

               <h1>
REMS TA Center CoP Portal Rules
    </h1>
    <p>
        The REMS TA Center Community of Practice (CoP) is a virtual space for schools, school districts, institutions of higher education (IHEs), and their community partners to collaborate on special projects, share news and resources, discuss trends and ideas, and learn from the experiences of others in the field. As a forum for practitioners in the field, content on the REMS TA Center CoP might come from various sources, and might be based on the experiences of CoP members and nonmembers. Statements, opinions, resources, or any other submission posted to the CoP by a member are not necessarily endorsed by the U.S. Department of Education or the REMS TA Center. 
    </p>
    <p>
        Please remember that the community will reflect and be enhanced by the diversity of those who support our nation’s schools and IHEs. At times, community members might not share the same opinion on a topic. Our Codes of Conduct and Terms of Use are designed to ensure that all community members receive respect from their fellow members while participating. Please read and accept our Codes of Conduct and Terms of Use to ensure a productive and healthy experience for all community members. 
    </p>
    <h2>CODES OF CONDUCT:</h2>
    <ul>
        <li><strong>Think twice before posting confidential or sensitive information</strong>. Our goal is to ensure the privacy and security of all CoP members. However, the REMS TA Center cannot guarantee that knowledge and information exchanged will not be shared outside the CoP. Protect yourself and others, and refrain from posting sensitive information that could cause harm if viewed outside of the CoP</li>
        <li><strong>You are accountable for messages posted to the CoP</strong>. To ensure that you (and not your school, district, IHE, or agency) are responsible for content posted, we require that all members use their real name and official affiliation. </li>
        <li><strong>A strong and diverse community requires mutual respect.</strong> The REMS TA Center encourages CoP members to participate in forum discussions and debates. We ask that the opinions and thoughts of all members be respected. Please also consider that the CoP is open to a diverse audience, and some members might not be communicating in their primary language.  </li>
    </ul>
    <h2>TERMS OF USE:</h2>
    <ol>
        <li>By submitting information on the REMS TA Center CoP, you grant to the U.S. Department of Education and the REMS TA Center a perpetual, worldwide, royalty-free, irrevocable, sub-licensable, nonexclusive right to use, reproduce, display, modify, distribute, and create derivative works for any purpose (in any format whatsoever) based on the submitted information.</li>
        <li>REMS TA Center CoP members are solely responsible for ensuring that they do not act in any manner that constitutes or forms a part of a course of conduct amounting to a violation of any state, federal, international, or other applicable law. This includes, but is not limited to, posting of content in violation of the copyright on that content.</li>
        <li>The REMS TA Center does not allow content that is abusive, vulgar, racist, sexist, slanderous, harassing, misleading, or otherwise objectionable. Content of this nature will be screened and removed. </li>
    
    
    </ol>
    <h2>
        TERMINATION:
    </h2>
    <p>
        The U.S. Department of Education and REMS TA Center may terminate or suspend your access to all or part of the REMS TA Center CoP, including but not limited to any discussion forums on its site, for any reason, including breach of the Terms of Use or Codes of Conduct. If you are unsatisfied with the services provided by the REMS TA Center, please email info@remstacenter.org to terminate your membership.
    </p>
    <h2>PRIVACY POLICY:</h2>
    <p>
The U.S. Department of Education and REMS TA Center will not sell any personal information about REMS TA Center CoP members to any third parties. The U.S. Department of Education and REMS TA Center may use account information generically in order to revise its services or present findings from the use of its services. 
</p>
    <asp:RadioButtonList ID="rblAccept" runat="server">
        <asp:ListItem Value="1">I accept the Terms of Use and Codes of Conduct.</asp:ListItem>
        <asp:ListItem Selected ="True" Value="0">I do not accept the Terms of Use and Codes of Conduct.</asp:ListItem>
    </asp:RadioButtonList>
    <asp:CustomValidator ID="CustomValidator5" ClientValidationFunction="ValidateChkList"
            runat="server" >You must accept our terms and condition.</asp:CustomValidator>
        </asp:WizardStep>


        <asp:CreateUserWizardStep ID="RegisterUserWizardStep" runat="server">
                <ContentTemplate>
                    <h2>
                        Create a New Account
                    </h2>
                    <p>
                        Use the form below to create a new account.
                    </p>
                    <p>
                        Passwords are required to be a minimum of <%= Membership.MinRequiredPasswordLength %> characters in length.
                    </p>
                    <span class="failureNotification">
                        <asp:Literal ID="ErrorMessage" runat="server"></asp:Literal>
                    </span>
                    <asp:ValidationSummary ID="RegisterUserValidationSummary" runat="server" CssClass="failureNotification" 
                         ValidationGroup="RegisterUserValidationGroup"/>
                    <div class="accountInfo">
                        <fieldset class="register">
                            <legend>Account Information</legend>
                            <table width="500">
                            <tr>
                            <td><asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label></td>
                            <td><asp:TextBox ID="UserName" runat="server" CssClass="textEntry"></asp:TextBox></td>
                            <td><asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" 
                                     CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="User Name is required." 
                                     ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator></td>
                            </tr>
                               <tr>
                            <td><asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">E-mail:</asp:Label></td>
                            <td><asp:TextBox ID="Email" runat="server" CssClass="textEntry" Enabled="false"></asp:TextBox></td>
                            <td><asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email" 
                                     CssClass="failureNotification" ErrorMessage="E-mail is required." ToolTip="E-mail is required." 
                                     ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator></td>
                            </tr>
                               <tr>
                            <td><asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label></td>
                            <td> <asp:TextBox ID="Password" runat="server" CssClass="passwordEntry" TextMode="Password"></asp:TextBox></td>
                            <td> <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" 
                                     CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required." 
                                     ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator></td>
                            </tr>
                               <tr>
                            <td><asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword">Confirm Password:</asp:Label></td>
                            <td> <asp:TextBox ID="ConfirmPassword" runat="server" CssClass="passwordEntry" TextMode="Password"></asp:TextBox></td>
                            <td> <asp:RequiredFieldValidator ControlToValidate="ConfirmPassword" CssClass="failureNotification" Display="Dynamic" 
                                     ErrorMessage="Confirm Password is required." ID="ConfirmPasswordRequired" runat="server" 
                                     ToolTip="Confirm Password is required." ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" 
                                     CssClass="failureNotification" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match."
                                     ValidationGroup="RegisterUserValidationGroup">*</asp:CompareValidator></td>
                            </tr>
                               <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            </tr>
                            </table>
                        </fieldset>
                        <p class="submitButton">
                            <asp:Button ID="CreateUserButton" runat="server" CommandName="MoveNext" Text="Create User" 
                                 ValidationGroup="RegisterUserValidationGroup"/>
                        </p>
                    </div>

    
                </ContentTemplate>
                <CustomNavigationTemplate>
                </CustomNavigationTemplate>
            </asp:CreateUserWizardStep>
             <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server">
              <ContentTemplate>
        <table border="0" style="font-size: 100%; font-family: Verdana" id="TABLE1" >
            <tr>
                <td align="center" colspan="2" style="font-weight: bold; color: white; background-color: #5d7b9d; height: 18px;">
                    Complete</td>
            </tr>
            <tr>
                <td>
                    Your account has been successfully created.<br />
                    <br />
                    <asp:Label ID="SubscribeLabel" runat="server" Text="Click Continue button to go to log in page."></asp:Label><br />
                    
                    </td>
            </tr>
            <tr>
                <td align="right" colspan="2">
                    &nbsp;<asp:Button ID="ContinueButton" OnClick="ContinueButton_Click" runat="server" BackColor="#FFFBFF" BorderColor="#CCCCCC"
                        BorderStyle="Solid" BorderWidth="1px" CausesValidation="False" CommandName="Continue"
                        Font-Names="Verdana" ForeColor="#284775" Text="Continue" ValidationGroup="CreateUserWizard1" />
                </td>
            </tr>
        </table>
    </ContentTemplate>
                </asp:CompleteWizardStep>
        </WizardSteps>
    </asp:CreateUserWizard>
</div>
  <asp:SqlDataSource ID="dsState" runat="server" 
        ConnectionString="<%$ ConnectionStrings:AspNetForumConnectionString %>" 
        SelectCommand="SELECT [StateId], [Location] FROM [States]"></asp:SqlDataSource>

</asp:Content>