﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _2_About : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
		if (Session["aspnetforumUserID"] == null)
        {
            lbtnloginout.Text = "Log In";
            lbtnCOPindex.Visible = false;
        }
        else
        {
            lbtnloginout.Text = "Log Out";
            lbtnCOPindex.Visible = true;
        }
    }
}